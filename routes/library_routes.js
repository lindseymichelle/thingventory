var express = require('express');
var router = express.Router();
var library_dal = require('../model/library_dal');


// View All Libraries
router.get('/all', function(req, res) {
    library_dal.getAll(function(allErr, allResult){
        if(allErr) {
            res.send(allErr);
        }
        else {
            library_dal.numberLibraries(function(numErr, numResult){
                if(numErr) {
                    res.send(numErr);
                } else {
                    library_dal.avgNumberObjects(function (avgErr, avgResult) {
                        if(avgErr) {
                            res.send(avgErr);
                        } else {
                            console.log('avg libraries: ', avgResult);
                            res.render('library/libraryViewAll', {
                                'result': allResult,
                                'numLibraries': numResult[0]['COUNT(library_id)'],
                                'recent_change': req.query.recent_change,
                                'avg_object' : avgResult[0]

                            });
                        }
                    });

                }
            });
        }
    });

});


// View the library for the given id
router.get('/', function(req, res){
    if(req.query.library_id === null) {
        res.send('library_id is null');
    } else {
        library_dal.getById(req.query.library_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('library/libraryViewById', {'result': result, 'library_id': req.query.library_id, 'recent_change': req.query.recent_change});
           }
        });
    }
});

// Return the add a new library form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    library_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('library/libraryAdd', {'result': result});
        }
    });
});

// View the library for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.library_name == null) {
        res.send('Library Name must be provided.');
    } else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        library_dal.insert(req.query, function(err,result) {
            if (err) {
                res.send(err);
            } else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                var change = "Library " + req.query.library_name + " Added";
                res.redirect(302, '/library/all?recent_change=' + change);
            }
        });
    }
});


router.get('/about', function(req, res){
    res.render('library/projectAbout', {title: 'About Thingventory'});
});



router.get('/edit', function(req, res){
    if(req.query.library_id === null) {
        res.send('A library id is required');
    }
    else {
        library_dal.edit(req.query.library_id, function(err, result){
            res.render('library/libraryUpdate', {library: result[0][0]});
        });
    }

});



router.get('/update', function(req, res) {
    library_dal.update(req.query, function(err, result){
        var change = "Library " + req.query.library_name + " Updated";
        res.redirect(302, '/library/all?recent_change=' + change);
    });
});

// Delete a library for the given library_id
router.get('/delete', function(req, res){
    if(req.query.library_id === null) {
        res.send('library__id is null');
    }
    else {
         library_dal.delete(req.query.library_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 var change = "Library " + req.query.library_name + " Deleted";
                 res.redirect(302, '/library/all?recent_change=' + change);
             }
         });
    }
});

module.exports = router;
