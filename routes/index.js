var express = require('express');
var router = express.Router();
var library_dal = require('../model/library_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
    library_dal.getAll(function(allErr, allResult){
        if(allErr) {
            res.send(allErr);
        }
        else {
            library_dal.numberLibraries(function(numErr, numResult){
                if(numErr) {
                    res.send(numErr);
                } else {
                    library_dal.avgNumberObjects(function (avgErr, avgResult) {
                        if(avgErr) {
                            res.send(avgErr);
                        } else {
                            console.log('avg libraries: ', avgResult);
                            res.render('library/libraryViewAll', {
                                'result': allResult,
                                'numLibraries': numResult[0]['COUNT(library_id)'],
                                'recent_change': null,
                                'avg_object' : avgResult[0]['AVG(objects)']
                            });
                        }
                    });

                }
            });
        }
    });
});

module.exports = router;
