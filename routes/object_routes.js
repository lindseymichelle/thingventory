var express = require('express');
var router = express.Router();
var object_dal = require('../model/object_dal');

// View the object for the given id
router.get('/', function(req, res){
    if(!req.query.object_id) {
        res.send('object_id is null');
    }
    else {
        object_dal.getById(req.query.object_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('object/objectViewById', {'result': result});
           }
        });
    }
});

// Return the add a new object form
router.get('/add', function(req, res){
    if (!req.query.library_id) {
        res.send('library_id is null');
    }
    else {
        res.render('object/objectAdd', {'library_id': req.query.library_id});
    }
});

// View the object for the given id
router.get('/insert', function(req, res){
    // simple validation
    var library_id = req.query.library_id;
    var object_name = req.query.object_name;
    if(!req.query.object_name || !library_id) {
        res.send('object_name and library_id must be provided.');
    } else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        object_dal.insert(req.query, function(err,result) {
            if (err) {
                res.send(err);
            } else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                var change = "Object " + object_name + " Added";
                res.redirect(302, '/library?library_id=' + library_id + "&recent_change=" + change);
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(!req.query.object_id || !req.query.library_id) {
        res.send('An object id and library id are required');
    }
    else {
        object_dal.edit(req.query.object_id, function(err, result){
            if (err) {
                res.send('Error getting object id ' + req.query.object_id + ': ' + err);
            }
            else {
                res.render('object/objectUpdate', {object: result[0][0], library_id: req.query.library_id});
            }
        });
    }

});

router.get('/update', function(req, res) {
    var library_id = req.query.library_id;
    var object_name = req.query.object_name;
    if(!library_id) {
        res.send('library_id must be provided.');
    } else {
        object_dal.update(req.query, function (err, result) {
            if (err) {
                res.send('Error editing object id ' + req.query.object_id + ': ' + err);
            }
            else {
                var change = "Object " + object_name + " Updated";
                res.redirect(302, '/library?library_id=' + library_id + "&recent_change=" + change);
            }
        });
    }
});

// Delete an object for the given object_id
router.get('/delete', function(req, res){
    var library_id = req.query.library_id;
    var object_name = req.query.object_name;
    if(!req.query.object_id || !library_id) {
        res.send('object_id and library_id are required');
    }
    else {
         object_dal.delete(req.query.object_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 var change = "Object " + object_name + " Deleted";
                 res.redirect(302, '/library?library_id=' + library_id + "&recent_change=" + change);
             }
         });
    }
});

module.exports = router;
