var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(library_id, callback) {
    var query = 'SELECT * FROM object WHERE library_id = ?';
    var queryData = [library_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(object_id, callback) {
    var query = 'SELECT * FROM object WHERE object_id = ?';
    var queryData = [object_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    var query = 'INSERT INTO object (library_id, object_name, quantity, unit_of_measure, ' +
        'color, composition) VALUES (?, ?, ?, ?, ?, ?) ';

    var queryData = [params.library_id, params.object_name, params.quantity, params.unit_of_measure,
                    params.color, params.composition];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);

    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE object SET object_name = ? , quantity = ? , ' +
        'unit_of_measure = ? , color = ? , composition = ? WHERE object_id = ?';
    var queryData = [params.object_name, params.quantity, params.unit_of_measure,
        params.color, params.composition, params.object_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};


exports.delete = function(object_id, callback) {
    var query = 'DELETE FROM object WHERE object_id = ?';
    var queryData = [object_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


/*
DROP PROCEDURE IF EXISTS object_getinfo;
DELIMITER //
CREATE PROCEDURE object_getinfo(_object_id int)
BEGIN
SELECT * FROM object
WHERE object_id = _object_id;

END //
DELIMITER ;
CALL object_getinfo (4);
*/

exports.edit = function(object_id, callback) {
    var query = 'CALL object_getinfo(?)';
    var queryData = [object_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


