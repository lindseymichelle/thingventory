var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view library_view as
 SELECT * FROM library;

 */
exports.getAll = function(callback) {
    var query = 'SELECT * FROM library ORDER BY library_name ASC';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.numberLibraries = function(callback) {
    var query = "SELECT COUNT(library_id) FROM library ";
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.avgNumberObjects = function(callback) {
    var query = "SELECT AVG(objects) FROM (SELECT l.library_id, " +
        "COUNT(o.object_id) AS objects FROM library l LEFT JOIN object o ON" +
        " l.library_id = o.library_id GROUP BY l.library_id) AS obj";
    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(library_id, callback) {
    var query = 'SELECT l.*, o.* FROM library l ' +
            'LEFT JOIN object o on l.library_id = o.library_id ' +
             'WHERE l.library_id = ? ORDER BY object_name';

    var queryData = [library_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE LIBRARY
    var query = 'INSERT INTO library (library_name) VALUES (?)';

    var queryData = [params.library_name];

    connection.query(query, params.library_name, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(library_id, callback) {
    var query = 'DELETE FROM library WHERE library_id = ?';
    var queryData = [library_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE library SET library_name = ? WHERE library_id = ?';
    var queryData = [params.library_name, params.library_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS library_getinfo;

 DELIMITER //
 CREATE PROCEDURE library_getinfo(_library_id int)
 BEGIN
 SELECT l.* FROM library l WHERE l.library_id = _library_id;
 END; //
 DELIMITER ;

     # Call the Stored Procedure
     CALL library_getinfo (4);

 */

exports.edit = function(library_id, callback) {
    var query = 'CALL library_getinfo(?)';
    var queryData = [library_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};